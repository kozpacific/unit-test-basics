package pl.pacific112.unit.basics._2_mockito;

import name.falgout.jeffrey.testing.junit5.MockitoExtension;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import pl.pacific112.unit.basics._2_mockito._1_example.SentenceReverseService;
import pl.pacific112.unit.basics._2_mockito._1_example.SentenceValidator;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

class SentenceReverseServiceTest {

    private SentenceReverseService testObj = new SentenceReverseService(new SentenceValidator());

    @Test
    @DisplayName("Should reverse sentence when given sentence is valid.")
    void shouldReverseSentence_whenSentenceIsValid() {
        // arrange
        String givenSentence = "Given long or short sentence";

        // act
        String result = testObj.reverseSentence(givenSentence);

        // assert
        assertThat(result).isEqualTo("sentence short or long Given");
    }
}
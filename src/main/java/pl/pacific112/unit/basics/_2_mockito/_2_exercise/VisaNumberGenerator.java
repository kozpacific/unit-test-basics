package pl.pacific112.unit.basics._2_mockito._2_exercise;

import pl.pacific112.unit.basics._2_mockito._2_exercise.model.Client;

import java.util.Random;

public class VisaNumberGenerator {

    public static final Random RANDOM = new Random();

    public String generateVisaNumber(Client person) {
        int id = RANDOM.nextInt(1000);
        return id + new StringBuilder(person.getName()).reverse().toString();
    }
}

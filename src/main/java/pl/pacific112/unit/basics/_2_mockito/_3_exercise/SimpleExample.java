package pl.pacific112.unit.basics._2_mockito._3_exercise;


import pl.pacific112.unit.basics._2_mockito._3_exercise.model.SimpleBook;
import pl.pacific112.unit.basics._2_mockito._3_exercise.model.SimpleRole;
import pl.pacific112.unit.basics._2_mockito._3_exercise.model.SimpleUser;

import java.util.List;

public class SimpleExample {

    public static void main(String[] args) {
        SimpleBookService simpleBookService = new SimpleBookService(new SimpleSecurityService());
        SimpleUser maciek = new SimpleUser(1, "Maciek", SimpleRole.ADMIN);
        SimpleUser anastazja = new SimpleUser(2, "Anastazja", SimpleRole.USER);

        SimpleBook wojnaPokoj = simpleBookService.addNewBook(anastazja, "Wojna i Pokój", "Nic, nie ma już nic pewnego, poza nicością wszystkiego tego, co rozumiem, i wielkością czegoś niepojętego, lecz najważniejszego.");
        SimpleBook mistrzMalgorzata = simpleBookService.addNewBook(anastazja, "Mistrz i Małgorzata", "Tak, człowiek jest śmiertelny, ale to jeszcze pół biedy. Najgorsze, że to, iż jest śmiertelny, okazuje się niespodziewanie, w tym właśnie sęk!");
        SimpleBook dziennikiGwiazdowe = simpleBookService.addNewBook(maciek, "Dzienniki gwiazdowe", "Dotychczasowe obserwacje pouczają, że miliony razy człowiek kopał kartofle, ale nie jest wykluczone, że jeden raz na miliard stanie się na odwrót, że kartofel będzie kopał człowieka.");

        simpleBookService.findBooks(maciek).forEach(System.out::println);
        System.out.println();
        simpleBookService.findBooks(anastazja).forEach(System.out::println);

        System.out.println();

        System.out.println(simpleBookService.getBookContent(anastazja, wojnaPokoj.getId()));
        System.out.println(simpleBookService.getBookContent(anastazja, mistrzMalgorzata.getId()));
        System.out.println(simpleBookService.getBookContent(anastazja, dziennikiGwiazdowe.getId()));

        System.out.println();

        System.out.println(simpleBookService.getBookContent(maciek, mistrzMalgorzata.getId()));
        System.out.println(simpleBookService.getBookContent(maciek, dziennikiGwiazdowe.getId()));
    }
}

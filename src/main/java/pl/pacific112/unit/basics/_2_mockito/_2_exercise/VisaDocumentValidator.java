package pl.pacific112.unit.basics._2_mockito._2_exercise;

import pl.pacific112.unit.basics._2_mockito._2_exercise.model.Client;

import java.util.Random;

public class VisaDocumentValidator {

    private static final Random RANDOM = new Random();

    public boolean hasAllRequiredDocuments(Client person) {
        return RANDOM.nextBoolean();
    }
}

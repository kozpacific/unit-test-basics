package pl.pacific112.unit.basics._2_mockito._2_exercise.model;

public class VisaSubmitException extends RuntimeException {

    public VisaSubmitException(Client client) {
        super("Visa application for " + client.getName() + " denied.");
    }
}

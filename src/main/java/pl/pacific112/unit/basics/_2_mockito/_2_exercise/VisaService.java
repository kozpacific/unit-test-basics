package pl.pacific112.unit.basics._2_mockito._2_exercise;

import pl.pacific112.unit.basics._2_mockito._2_exercise.model.*;

public class VisaService {

    private final VisaNumberGenerator visaNumberGenerator;
    private final VisaDocumentValidator visaDocumentValidator;
    private final VisaDocumentSubmitService visaDocumentSubmitService;

    public VisaService(VisaNumberGenerator visaNumberGenerator, VisaDocumentValidator visaDocumentValidator, VisaDocumentSubmitService visaDocumentSubmitService) {
        this.visaNumberGenerator = visaNumberGenerator;
        this.visaDocumentValidator = visaDocumentValidator;
        this.visaDocumentSubmitService = visaDocumentSubmitService;
    }

    public VisaResult applyForVisa(Client p) {
        Client person = new Client(p.getName(), p.getDocuments());

        if (!visaDocumentValidator.hasAllRequiredDocuments(person)) {
            return VisaResult.denied(VisaSubmitStatus.MISSING_DOCUMENTS);
        }

        try {
            Visa visa = submitDocumentsForVisa(person);
            return VisaResult.approved(visa);
        } catch (VisaSubmitException ex) {
            System.out.println(ex.getMessage());
            return VisaResult.denied(VisaSubmitStatus.DENIED);
        }
    }

    private Visa submitDocumentsForVisa(Client person) {
        visaDocumentSubmitService.submitDocuments(person);
        String visaId = visaNumberGenerator.generateVisaNumber(person);
        return new Visa(visaId, person);
    }
}

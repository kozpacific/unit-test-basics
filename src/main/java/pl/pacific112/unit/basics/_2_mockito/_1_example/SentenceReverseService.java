package pl.pacific112.unit.basics._2_mockito._1_example;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static java.util.stream.Collectors.joining;

public class SentenceReverseService {

    private final SentenceValidator sentenceValidator;

    public SentenceReverseService(SentenceValidator sentenceValidator) {
        this.sentenceValidator = sentenceValidator;
    }

    public String reverseSentence(String sentence) {
        if (sentenceValidator.isValidSentence(sentence)) {
            List<String> words = Arrays.asList(sentence.split(" "));
            Collections.reverse(words);

            return String.join(" ", words);
        }

        throw new IllegalArgumentException("Sentence '" + sentence + "' is incorrect");
    }
}

package pl.pacific112.unit.basics._1_basics._1_example;

import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class StringReverseService {

    public String reverseString(String toReverse) {
        return IntStream.rangeClosed(1, toReverse.length())
                .map(i -> toReverse.length() - i)
                .mapToObj(toReverse::charAt)
                .map(String::valueOf)
                .collect(Collectors.joining());
    }
}

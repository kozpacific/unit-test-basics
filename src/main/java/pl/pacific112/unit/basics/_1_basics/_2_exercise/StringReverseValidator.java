package pl.pacific112.unit.basics._1_basics._2_exercise;

public class StringReverseValidator {

    public boolean canReverseString(String toReverse) {
        return !toReverse.isEmpty() && containsMultipleCharacters(toReverse);
    }

    private boolean containsMultipleCharacters(String toCheck) {
        return toCheck.codePoints().distinct().count() != 1;
    }
}

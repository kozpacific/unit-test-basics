package pl.pacific112.unit.basics._2_mockito._2_exercise;

import pl.pacific112.unit.basics._2_mockito._2_exercise.model.Client;
import pl.pacific112.unit.basics._2_mockito._2_exercise.model.VisaSubmitException;
import pl.pacific112.unit.basics._2_mockito._2_exercise.model.VisaSubmitStatus;

import java.util.Random;

public class VisaDocumentSubmitService {

    private static final Random RANDOM = new Random();

    public VisaSubmitStatus submitDocuments(Client person) {
        if (RANDOM.nextBoolean()) {
            throw new VisaSubmitException(person);
        }

        return VisaSubmitStatus.APPROVED;
    }

}

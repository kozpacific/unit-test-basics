package pl.pacific112.unit.basics._3_exercise;

// TODO: Zaimplementować kalkulator z funkcją dzielenia, który jako input przyjmuje String z liczbami po czym wykonuje działanie
 // na podanych liczbach. Np.
 //        new StringCalculator().divide("16,4,2") da wynik 2
 // Kalkulator obsługiwać będzie liczby ujemne oraz zmiennoprzecinkowe
 // Założenie:
 //  - liczby oddzielone będą przecinkami
 // Wymagania:
 //  - Działanie wykonywane jest na liczbach od lewej do prawej
 //  - String powinien zawierać tylko liczby oddzielone przecinkami - jeżeli warunek nie jest spełniony rzucony zostanie wyjątek z dokładnym opisem który znak jest niepoprawny
 //  - Jeżeli String zawiera mniej niż jedną liczbę rzucony zostanie wyjątek z wiadomością - String should contain at least two numbers
 //  - Jeżeli któraś z dzielnych będzie wynosić zero rzucony zostanie wyjątek z wiadomości - Con't divide by zero
class StringCalculatorTest {

}
package pl.pacific112.unit.basics._2_mockito._2_exercise.model;

public class Visa {

    private final String number;
    private final Client client;

    public Visa(String number, Client client) {
        this.number = number;
        this.client = client;
    }

    public String getNumber() {
        return number;
    }

    public Client getClient() {
        return client;
    }

    @Override
    public String toString() {
        return "Visa{" +
                "number='" + number + '\'' +
                ", client=" + client +
                '}';
    }
}

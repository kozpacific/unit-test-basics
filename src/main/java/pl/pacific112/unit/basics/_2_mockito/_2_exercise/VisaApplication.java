package pl.pacific112.unit.basics._2_mockito._2_exercise;


import pl.pacific112.unit.basics._2_mockito._2_exercise.model.Client;
import pl.pacific112.unit.basics._2_mockito._2_exercise.model.VisaDocumentType;

import java.util.Arrays;

public class VisaApplication {

    // TODO: Napisać testy do klasy Visa Service
    public static void main(String[] args) {
        VisaService visaService = new VisaService(new VisaNumberGenerator(), new VisaDocumentValidator(), new VisaDocumentSubmitService());

        Client maciek = new Client("Maciek", Arrays.asList(VisaDocumentType.BIRTH_CERTIFICATE, VisaDocumentType.PASSPORT));

        System.out.println(visaService.applyForVisa(maciek));
        System.out.println(visaService.applyForVisa(maciek));
        System.out.println(visaService.applyForVisa(maciek));
        System.out.println(visaService.applyForVisa(maciek));
        System.out.println(visaService.applyForVisa(maciek));
    }

}

package pl.pacific112.unit.basics._2_mockito._3_exercise.model;

import java.awt.print.Book;
import java.util.Objects;

public class SimpleBook {

    private final long id;
    private final String title;
    private final String content;
    private final long ownerId;

    public SimpleBook(long id, String title, String content, long ownerId) {
        this.id = id;
        this.title = title;
        this.content = content;
        this.ownerId = ownerId;
    }

    public long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getContent() {
        return content;
    }

    public long getOwnerId() {
        return ownerId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SimpleBook that = (SimpleBook) o;
        return id == that.id &&
                ownerId == that.ownerId &&
                Objects.equals(title, that.title) &&
                Objects.equals(content, that.content);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, title, content, ownerId);
    }

    @Override
    public String toString() {
        return "SimpleBook{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", content='" + content + '\'' +
                ", ownerId=" + ownerId +
                '}';
    }
}

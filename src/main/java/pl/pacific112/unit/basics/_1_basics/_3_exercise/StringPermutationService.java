package pl.pacific112.unit.basics._1_basics._3_exercise;

import java.util.HashSet;
import java.util.Set;

public class StringPermutationService {

    public Set<String> generateStringPermutations(String input) {
        return generatePermutation(input);
    }

    private Set<String> generatePermutation(String str) {
        Set<String> perm = new HashSet<>();

        if (str == null) {
            return null;
        } else if (str.length() == 0) {
            perm.add("");
            return perm;
        }
        char initial = str.charAt(0);
        String rem = str.substring(1);
        Set<String> words = generatePermutation(rem);
        for (String strNew : words) {
            for (int i = 0; i <= strNew.length(); i++) {
                perm.add(charInsert(strNew, initial, i));
            }
        }
        return perm;
    }

    private String charInsert(String str, char c, int j) {
        String begin = str.substring(0, j);
        String end = str.substring(j);
        return begin + c + end;
    }
}

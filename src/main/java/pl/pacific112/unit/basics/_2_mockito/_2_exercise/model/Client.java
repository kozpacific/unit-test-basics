package pl.pacific112.unit.basics._2_mockito._2_exercise.model;

import java.util.List;
import java.util.Objects;

public class Client {

    private final String name;
    private final List<VisaDocumentType> documents;

    public Client(String name, List<VisaDocumentType> documents) {
        this.name = name;
        this.documents = documents;
    }

    public String getName() {
        return name;
    }

    public List<VisaDocumentType> getDocuments() {
        return documents;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Client client = (Client) o;
        return Objects.equals(name, client.name) &&
                Objects.equals(documents, client.documents);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, documents);
    }

    @Override
    public String toString() {
        return "Client{" +
                "name='" + name + '\'' +
                ", documents=" + documents +
                '}';
    }
}

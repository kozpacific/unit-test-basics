package pl.pacific112.unit.basics._2_mockito._3_exercise;


import pl.pacific112.unit.basics._2_mockito._3_exercise.model.SimpleBook;
import pl.pacific112.unit.basics._2_mockito._3_exercise.model.SimpleUser;

import java.util.Objects;

public class SimpleSecurityService {

    public boolean hasAccess(SimpleUser user, SimpleBook book) {
        if (user.isAdmin()) {
            return true;
        }

        return Objects.equals(user.getId(), book.getOwnerId());
    }
}

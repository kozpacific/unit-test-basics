package pl.pacific112.unit.basics._1_basics._1_example;

public class StringReverseApplication {

    public static void main(String[] args) {
        String toReverse = "Maciek";
        StringReverseService reverseService = new StringReverseService();

        String reversedString = reverseService.reverseString(toReverse);
        System.out.println(reversedString);
    }
}
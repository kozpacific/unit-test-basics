package pl.pacific112.unit.basics._2_mockito._1_example;

public class SentenceReverseApplication {

    public static void main(String[] args) {
        String givenSentence = "This is sentence";

        SentenceReverseService sentenceReverseService = new SentenceReverseService(new SentenceValidator());
        String result = sentenceReverseService.reverseSentence(givenSentence);
        System.out.println(result);
    }
}

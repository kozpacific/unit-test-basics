package pl.pacific112.unit.basics._2_mockito._2_exercise.model;

public enum VisaSubmitStatus {

    APPROVED, DENIED, MISSING_DOCUMENTS

}

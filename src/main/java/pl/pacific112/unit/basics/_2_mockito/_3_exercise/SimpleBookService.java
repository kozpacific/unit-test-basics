package pl.pacific112.unit.basics._2_mockito._3_exercise;


import pl.pacific112.unit.basics._2_mockito._3_exercise.model.SimpleBook;
import pl.pacific112.unit.basics._2_mockito._3_exercise.model.SimpleUser;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.stream.Collectors;

public class SimpleBookService {

    private static final List<SimpleBook> books = new ArrayList<>();

    private final SimpleSecurityService simpleSecurityService;

    public SimpleBookService(SimpleSecurityService simpleSecurityService) {
        this.simpleSecurityService = simpleSecurityService;
    }

    public List<SimpleBook> findBooks(SimpleUser user) {
        return books.stream()
                .filter(b -> simpleSecurityService.hasAccess(user, b))
                .collect(Collectors.toList());
    }

    public String getBookContent(SimpleUser user, long bookId) {
        return findById(user, bookId)
                .map(b -> String.format("Title %s, content: %s", b.getTitle(), b.getContent()))
                .orElse("!!! Brak dostępu !!!");
    }

    public SimpleBook addNewBook(SimpleUser user, String title, String content) {
        SimpleBook bookToSave = new SimpleBook(new Random().nextInt(100_000), title, content, user.getId());
        books.add(bookToSave);

        return bookToSave;
    }

    private Optional<SimpleBook> findById(SimpleUser user, long bookId) {
        return books.stream()
                .filter(b -> simpleSecurityService.hasAccess(user, b))
                .filter(b -> b.getId() == bookId)
                .findFirst();
    }
}

package pl.pacific112.unit.basics._1_basics._3_exercise;

import java.util.Set;

public class StringPermutationApplication {

    public static void main(String[] args) {
        StringPermutationService permutationService = new StringPermutationService();
        Set<String> result = permutationService.generateStringPermutations("abc");

        System.out.println(result);
    }
}

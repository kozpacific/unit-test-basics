package pl.pacific112.unit.basics._2_mockito._2_exercise.model;

import java.util.Optional;

public class VisaResult {

    private final Visa visa;
    private final VisaSubmitStatus status;

    public VisaResult(Visa visa, VisaSubmitStatus status) {
        this.visa = visa;
        this.status = status;
    }

    public static VisaResult approved(Visa visa) {
        return new VisaResult(visa, VisaSubmitStatus.APPROVED);
    }

    public static VisaResult denied(VisaSubmitStatus status) {
        return new VisaResult(null, status);
    }

    public Optional<Visa> getVisa() {
        return Optional.ofNullable(visa);
    }

    public VisaSubmitStatus getStatus() {
        return status;
    }

    @Override
    public String toString() {
        return "VisaResult{" +
                "visa=" + visa +
                ", status=" + status +
                '}';
    }
}

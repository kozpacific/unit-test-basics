package pl.pacific112.unit.basics._2_mockito._1_example;

public class SentenceValidator {

    public boolean isValidSentence(String sentence) {
        return hasMoreThanOneWord(sentence) && sentence.endsWith(".");
    }

    private boolean hasMoreThanOneWord(String sentence) {
        return sentence.split(" ").length > 1;
    }
}
package pl.pacific112.unit.basics._2_mockito._3_exercise.model;

import java.util.List;
import java.util.Objects;

public class SimpleUser {

    private final long id;
    private final String userName;
    private final SimpleRole role;

    public SimpleUser(long id, String userName, SimpleRole role) {
        this.id = id;
        this.userName = userName;
        this.role = role;
    }

    public long getId() {
        return id;
    }

    public String getUserName() {
        return userName;
    }

    public SimpleRole getRole() {
        return role;
    }

    public boolean isAdmin() {
        return role == SimpleRole.ADMIN;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SimpleUser that = (SimpleUser) o;
        return id == that.id &&
                Objects.equals(userName, that.userName) &&
                role == that.role;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, userName, role);
    }

    @Override
    public String toString() {
        return "SimpleUser{" +
                "id=" + id +
                ", userName='" + userName + '\'' +
                ", role=" + role +
                '}';
    }
}

package pl.pacific112.unit.basics._2_mockito;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import pl.pacific112.unit.basics._2_mockito._1_example.SentenceValidator;

import static org.assertj.core.api.Assertions.assertThat;

class SentenceValidatorTest {

    private final SentenceValidator testObj = new SentenceValidator();

    @Test
    @DisplayName("Should return true when sentence has more than one word")
    void shouldReturnTrue_whenSentenceHasMoreThanOneWork() {
        // arrange
        String givenSentence = "Valid sentence";

        // act
        boolean result = testObj.isValidSentence(givenSentence);

        // assert
        assertThat(result).isTrue();
    }

    @Test
    @DisplayName("Should return false when sentence has exactly one word")
    void shouldReturnFalse_whenSentenceHasExactlyOneWord() {
        // arrange
        String givenSentence = "Invalid";

        // act
        boolean result = testObj.isValidSentence(givenSentence);

        // assert
        assertThat(result).isFalse();
    }
}